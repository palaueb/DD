# Contribuïnt

DD és programari lliure sota la llicència AGPL3+, les contribucions de qualsevol
persona són benvingudes.

El repositori de codi es troba a:
[https://gitlab.com/DD-workspace/DD](https://gitlab.com/DD-workspace/DD)
