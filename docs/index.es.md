# DD handbook

DD es el workspace educativo generado en el marco del Plan de Digitalización Democrática de Xnet. Ha sido creado y powered por
Xnet, familias y centros promotores, IsardVDI, 3ipunt, MaadiX, eXO.cat, Evilham y financiado por la Dirección de Innovación
Democrática, Comisionado de Innovación Digital, Comisionado de Economía Social del Ayuntamiento de Barcelona, en
colaboración con el Consorcio de Educación de Barcelona, aFFaC y AirVPN.

La aplicación DD puede utilizarse libremente siempre y cuando conste este footer y se respete la licencia [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html).

# ¿Qué es DD?

DD configura un proveedor de indentidad y muchas aplicaciones para ofrecer una
experiencia de usuario cohesiva, considerando escuelas y universidades como el
caso de uso principal.

Este proyecto proporciona una solución integrada para gestionar entornos
educativos como:

- **Aulas**: Una instancia de Moodle con tema y conectores personalizados
- **Archivos**: Una instancia de Nextcloud con tema y conectores personalizados
- **Documentos**: Un visor y editor de documentos integrado con Nextcloud
- **Páginas web**: Una instancia de WordPress con tema y conectores personalitzados
- **Pad**: Una instancia Etherpad integrada con Nextcloud
- **Conferencias**: Un BigBlueButton integrado con Moodle y Nextcloud (necesita servidor independiente)
- **Formularios**: Conector de Nextcloud para formularios

|                              |                                 |
| ---------------------------- | ------------------------------- |
| ![](img/classrooms.png) | ![](img/cloud_storage.png) |

# Interfaz de administración

Este proyecto incluye una interfaz de administración que permite administrar
fácilmente usuarios y grupos para mantenerlos sincronizados entre las diferentes
aplicaciones.

| ![](img/admin_sync.png) | ![](img/admin_user_edit.png) |
| ---------------------------- | --------------------------------- |

Para migrar e introducir usuarios y grupos al sistema de forma sencilla hay dos
caminos de importación:

- Desde la consola de administración de Google con un fichero JSON
- Desde un archivo CSV

# ¡Me interesa!

¡Genial! Tanto si quieres contribuir como si tienes interés en desplegar DD para
tu organización, nos alegrará hablar contigo.
Aquí tienes algunos recursos para guiarte más:

- [Manual de usuario DD](https://dd.digitalitzacio-democratica.xnet-x.net/manual-usuari/)
- [Instalación](install.ca.md)
- [Post-instalación](post-install.ca.md)
- [Código fuente](https://gitlab.com/DD-workspace/DD)

Página creada con [MkDocs](https://gitlab.com/pages/mkdocs).
Puedes [ver y modificar su código](https://gitlab.com/DD-workspace/DD).
