# DD

DD is the education workspace generated within the framework of Xnet's Democratic Digitalisation Plan. It has been created and powered by
Xnet, families and promoting centres, IsardVDI, 3iPunt, MaadiX, eXO.cat, Evilham and funded by the Directorate for
Democratic Innovation, the Barcelona City Council's Digital Innovation Commissioner, Social Economy Commissioner, in
collaboration with the Barcelona Education Consortium, aFFaC and AirVPN.

DD can be used freely as long as this footer is included and the [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html) license is respected.

# What is DD?

DD sets up an identity provider and many apps providing a cohesive user
experience considering schools and universities as the main use-case.

The project provides an integrated solution to handle the common
environment in education:

- **Classrooms**: A Moodle instance with custom theme and custom plugins
- **Cloud drive**: A Nextcloud instance with custom theme and custom plugins
- **Documents**: A document viewer and editor integrated with Nextcloud
- **Web pages**: A Wordpress instance with custom theme and custom plugins
- **Pad**: An Etherpad instance integrated with Nextcloud
- **Conferences**: BigBlueButton integrated with Moodle and Nextcloud (needs a standalone host)
- **Forms**: A forms Nextcloud plugin

|                              |                                 |
| ---------------------------- | ------------------------------- |
| ![](img/classrooms.png) | ![](img/cloud_storage.png) |

## Administration interface

The project includes an administration interface that allows to easily manage
users and groups and keep these in sync between applications.

| ![](img/admin_sync.png) | ![](img/admin_user_edit.png) |
| ---------------------------- | --------------------------------- |

To easily migrate and insert users and groups to the system there are also two
provided imports:

- From Google Admin Console as a JSON dump
- From a CSV file

# I'm interested!

That's great! Whether you want to contribute or are interested in deploying DD
for your organisation, we'll be happy to hear from you, here are some
resources to aid you further:

- [User handbook](https://dd.digitalitzacio-democratica.xnet-x.net/manual-usuari/)
- [Installation](install.ca.md)
- [Post-install](post-install.ca.md)
- [Source code](https://gitlab.com/DD-workspace/DD)

This site is built with [MkDocs](https://gitlab.com/pages/mkdocs).
You can [browse and modify its source code](https://gitlab.com/DD-workspace/DD).
