#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import os

new_file = ""
with open("select.html") as f:
    for line in f:
        icon = line.split("'")[1]
        icon_hex = line.split(">")[1].split(";")[0]
        new_file += (
            "<option value='fa "
            + icon
            + "' {% if menu_item.icon == 'fa'"
            + icon
            + "' %} selected='selected'{% endif %}>"
            + icon_hex
            + "; "
            + icon
            + "</option>\n"
        )

with open("select_output.html", "w") as f:
    f.writelines(new_file)
