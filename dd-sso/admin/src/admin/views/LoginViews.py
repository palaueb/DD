#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
import os

from flask import flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required, login_user, logout_user

from admin import app

from ..auth.authentication import *


@app.route("/", methods=["GET", "POST"])
@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        if request.form["user"] == "" or request.form["password"] == "":
            flash("Can't leave it blank", "danger")
        elif request.form["user"].startswith(" "):
            flash("Username not found or incorrect password.", "warning")
        else:
            ram_user = ram_users.get(request.form["user"])
            if ram_user and request.form["password"] == ram_user["password"]:
                user = User(
                    {
                        "id": ram_user["id"],
                        "password": ram_user["password"],
                        "role": ram_user["role"],
                        "active": True,
                    }
                )
                login_user(user)
                flash("Logged in successfully.", "success")
                return redirect(url_for("web_users"))
            else:
                flash("Username not found or incorrect password.", "warning")
    return render_template("login.html")


@app.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for("login"))
