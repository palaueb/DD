#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import logging as log
import os

from flask import Flask, render_template, send_from_directory

app = Flask(__name__, static_url_path="")
app = Flask(__name__, template_folder="static/templates")
app.url_map.strict_slashes = False

"""
App secret key for encrypting cookies
You can generate one with:
    import os
    os.urandom(24)
And paste it here.
"""
app.secret_key = "Change this key!/\xf7\x83\xbe\x17\xfa\xa3zT\n\\]m\xa6\x8bF\xdd\r\xf7\x9e\x1d\x1f\x14'"

print("Starting dd-sso api...")

from admin.lib.load_config import loadConfig

try:
    loadConfig(app)
except:
    print("Could not get environment variables...")

from admin.lib.postup import Postup

Postup()

from admin.lib.admin import Admin

app.admin = Admin()

app.ready = False

"""
Debug should be removed on production!
"""
if app.debug:
    log.warning("Debug mode: {}".format(app.debug))
else:
    log.info("Debug mode: {}".format(app.debug))

"""
Serve static files
"""


@app.route("/build/<path:path>")
def send_build(path):
    return send_from_directory(
        os.path.join(app.root_path, "node_modules/gentelella/build"), path
    )


@app.route("/vendors/<path:path>")
def send_vendors(path):
    return send_from_directory(
        os.path.join(app.root_path, "node_modules/gentelella/vendors"), path
    )


@app.route("/node_modules/<path:path>")
def send_nodes(path):
    return send_from_directory(os.path.join(app.root_path, "node_modules"), path)


@app.route("/templates/<path:path>")
def send_templates(path):
    return send_from_directory(os.path.join(app.root_path, "templates"), path)


# @app.route('/templates/<path:path>')
# def send_templates(path):
#     return send_from_directory(os.path.join(app.root_path, 'static/templates'), path)


@app.route("/static/<path:path>")
def send_static_js(path):
    return send_from_directory(os.path.join(app.root_path, "static"), path)


@app.route("/avatars/<path:path>")
def send_avatars_img(path):
    return send_from_directory(
        os.path.join(app.root_path, "../avatars/master-avatars"), path
    )


@app.route("/custom/<path:path>")
def send_custom(path):
    return send_from_directory(os.path.join(app.root_path, "../custom"), path)


# @app.errorhandler(404)
# def not_found_error(error):
#     return render_template('page_404.html'), 404

# @app.errorhandler(500)
# def internal_error(error):
#     return render_template('page_500.html'), 500

"""
Import all views
"""
from .views import ApiViews, AppViews, LoginViews, WebViews, WpViews
