#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later


import logging as log
import os
import sys
import traceback

import yaml
from cerberus import Validator, rules_set_registry, schema_registry

from admin import app


class AdminValidator(Validator):
    None
    # def _normalize_default_setter_genid(self, document):
    #     return _parse_string(document["name"])

    # def _normalize_default_setter_genidlower(self, document):
    #     return _parse_string(document["name"]).lower()

    # def _normalize_default_setter_gengroupid(self, document):
    #     return _parse_string(
    #         document["parent_category"] + "-" + document["uid"]
    #     ).lower()


def load_validators(purge_unknown=True):
    validators = {}
    schema_path = os.path.join(app.root_path, "schemas")
    for schema_filename in os.listdir(schema_path):
        try:
            with open(os.path.join(schema_path, schema_filename)) as file:
                schema_yml = file.read()
                schema = yaml.load(schema_yml, Loader=yaml.FullLoader)
                validators[schema_filename.split(".")[0]] = AdminValidator(
                    schema, purge_unknown=purge_unknown
                )
        except IsADirectoryError:
            None
    return validators


app.validators = load_validators()


class loadConfig:
    def __init__(self, app=None):
        try:
            app.config.setdefault("DOMAIN", os.environ["DOMAIN"])
            app.config.setdefault(
                "KEYCLOAK_POSTGRES_USER", os.environ["KEYCLOAK_DB_USER"]
            )
            app.config.setdefault(
                "KEYCLOAK_POSTGRES_PASSWORD", os.environ["KEYCLOAK_DB_PASSWORD"]
            )
            app.config.setdefault(
                "MOODLE_POSTGRES_USER", os.environ["MOODLE_POSTGRES_USER"]
            )
            app.config.setdefault(
                "MOODLE_POSTGRES_PASSWORD", os.environ["MOODLE_POSTGRES_PASSWORD"]
            )
            app.config.setdefault(
                "NEXTCLOUD_POSTGRES_USER", os.environ["NEXTCLOUD_POSTGRES_USER"]
            )
            app.config.setdefault(
                "NEXTCLOUD_POSTGRES_PASSWORD", os.environ["NEXTCLOUD_POSTGRES_PASSWORD"]
            )
            app.config.setdefault(
                "VERIFY", True if os.environ["VERIFY"] == "true" else False
            )
            app.config.setdefault("API_SECRET", os.environ.get("API_SECRET"))
        except Exception as e:
            log.error(traceback.format_exc())
            raise
