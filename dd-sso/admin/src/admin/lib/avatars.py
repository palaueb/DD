#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
import logging as log
import os
from pprint import pprint

from minio import Minio
from minio.commonconfig import REPLACE, CopySource
from minio.deleteobjects import DeleteObject
from requests import get, post

from admin import app


class Avatars:
    def __init__(self):
        self.mclient = Minio(
            "dd-sso-avatars:9000",
            access_key="AKIAIOSFODNN7EXAMPLE",
            secret_key="wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY",
            secure=False,
        )
        self.bucket = "master-avatars"
        self._minio_set_realm()
        # self.update_missing_avatars()

    def add_user_default_avatar(self, userid, role="unknown"):
        self.mclient.fput_object(
            self.bucket,
            userid,
            os.path.join(app.root_path, "../custom/avatars/" + role + ".jpg"),
            content_type="image/jpeg ",
        )
        log.warning(
            " AVATARS: Updated avatar for user " + userid + " with role " + role
        )

    def delete_user_avatar(self, userid):
        self.minio_delete_object(userid)

    def update_missing_avatars(self, users):
        sys_roles = ["admin", "manager", "teacher", "student"]
        for u in self.get_users_without_image(users):
            try:
                img = [r + ".jpg" for r in sys_roles if r in u["roles"]][0]
            except:
                img = "unknown.jpg"

            self.mclient.fput_object(
                self.bucket,
                u["id"],
                os.path.join(app.root_path, "../custom/avatars/" + img),
                content_type="image/jpeg ",
            )
            log.warning(
                " AVATARS: Updated avatar for user "
                + u["username"]
                + " with role "
                + img.split(".")[0]
            )

    def _minio_set_realm(self):
        if not self.mclient.bucket_exists(self.bucket):
            self.mclient.make_bucket(self.bucket)

    def minio_get_objects(self):
        return [o.object_name for o in self.mclient.list_objects(self.bucket)]

    def minio_delete_all_objects(self):
        delete_object_list = map(
            lambda x: DeleteObject(x.object_name),
            self.mclient.list_objects(self.bucket),
        )
        errors = self.mclient.remove_objects(self.bucket, delete_object_list)
        for error in errors:
            log.error(" AVATARS: Error occured when deleting avatar object: " + error)

    def minio_delete_object(self, oid):
        errors = self.mclient.remove_objects(self.bucket, [DeleteObject(oid)])
        for error in errors:
            log.error(" AVATARS: Error occured when deleting avatar object: " + error)

    def get_users_without_image(self, users):
        return [u for u in users if u["id"] and u["id"] not in self.minio_get_objects()]
