    <footer id="page-footer">
        <div class="container2">
            <div class="footer-desc">
                <div class="footer-desc-logo">
                    <img class="footer-desc-logo-img" src="${url.resourcesPath}/img/logo_icon.svg" style=""/>                    
                </div>
                <div class="footer-desc-text">
                    <img class="footer-desc-img" src="${url.resourcesPath}/img/logo_text_dd.svg" style=""/>
                    <p class="footer-desc-par">DD és el workspace educatiu generat en el marc del Pla de Digitalització Democràtica d’Xnet. Ha estat creat i powered per Xnet, famílies i centres promotors, IsardVDI, 3iPunt, Direcció d’Innovació Democràtica, Direcció d’Innovació Digital, Comissionat d’Economia Social de l’Ajuntament de Barcelona, Consorci d’Educació de Barcelona. En col·laboració amb aFFaC i AirVPN.</p>                 
                </div>
                <div class="footer-desc-comma">
                    <img class="footer-desc-logo-comma" src="${url.resourcesPath}/img/dd_coma.svg" style=""/>                    
                </div>
            </div>
            <div class="footer-links">
                <div class="footer-links-item">
                    <img class="footer-desc-logo-comma-bullet" src="${url.resourcesPath}/img/dd_coma.svg" width="10"><a href="https://www.gnu.org/licenses/agpl-3.0.en.html" target="_blank">L’eina DD té una llicència AGPLv3</a>                
                </div>                
                <div class="footer-links-item">
                    <img class="footer-desc-logo-comma-bullet" src="${url.resourcesPath}/img/dd_coma.svg" width="10"/><a href="https://gitlab.com/DD-workspace/DD" target="_blank">Repositori DD a GitLab</a>
                </div>                
                <div class="footer-links-item">
                    <img class="footer-desc-logo-comma-bullet" src="${url.resourcesPath}/img/dd_coma.svg" width="10"/><a href="https://xnet-x.net/ca/digital-democratic/" target="_blank">Web Pla de Digitalització Democràtica</a>                
                </div>
                <div class="footer-links-policies">
                    <a href="https://admin.${properties.ddDomain}/legal_text?lang=ca" target="_blank">Avís legal del centre</a>
                </div>
            </div>
        </div>
    </footer>
