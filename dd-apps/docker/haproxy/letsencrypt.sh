#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
RENEW=0
if [[ ! -f /certs/chain.pem && ! -z "$LETSENCRYPT_EMAIL" && ! -z "$LETSENCRYPT_DNS" ]]; then
       	/usr/bin/certbot certonly --standalone -d "$LETSENCRYPT_DNS" -m "$LETSENCRYPT_EMAIL" -n --agree-tos
        if [[ $? == 0 ]] ; then
                cat /etc/letsencrypt/live/$LETSENCRYPT_DNS/fullchain.pem /etc/letsencrypt/live/$LETSENCRYPT_DNS/privkey.pem > /certs/chain.pem
                chmod 440 /certs/chain.pem
                mkdir -p /certs/letsencrypt/$LETSENCRYPT_DNS
                cp /etc/letsencrypt/live/$LETSENCRYPT_DNS/* /certs/letsencrypt/$LETSENCRYPT_DNS/
		RENEW=1
	fi
fi

if [ $RENEW == 1 ]; then
	/bin/sh -c '/letsencrypt-check.sh' &
fi
