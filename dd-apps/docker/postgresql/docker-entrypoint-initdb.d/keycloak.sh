#!/bin/bash
#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER $KEYCLOAK_DB_USER SUPERUSER PASSWORD '$KEYCLOAK_DB_PASSWORD';
    CREATE DATABASE $KEYCLOAK_DB_DATABASE;
    GRANT ALL PRIVILEGES ON DATABASE $KEYCLOAK_DB_DATABASE TO $KEYCLOAK_DB_USER;
EOSQL
